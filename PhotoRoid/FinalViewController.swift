//
//  FinalViewController.swift
//  PhotoRoid
//
//  Created by Wagner Rodrigues on 06/02/2018.
//  Copyright © 2018 Wagner Rodrigues. All rights reserved.
//

import UIKit
import Photos

class FinalViewController: UIViewController {
    
    @IBOutlet weak var ivPhoto: UIImageView!

    var image: UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()
        ivPhoto.image = image
        ivPhoto.layer.borderWidth = 10
        ivPhoto.layer.borderColor = UIColor.white.cgColor

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveToAlbum() {
        PHPhotoLibrary.shared().performChanges({
            let creation = PHAssetChangeRequest.creationRequestForAsset(from: self.image)
            let addAssetRequest = PHAssetCollectionChangeRequest()
            addAssetRequest.addAssets([creation.placeholderForCreatedAsset]as NSArray)
            
        }) { (success, erro) in
            if !success {
                print(erro?.localizedDescription)
            } else {
                let alert = UIAlertController(title: "Imagem salva  ", message: "Sua imagem foi salva no álbum de fotos", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func save(_ sender: UIButton) {
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized:
                self.saveToAlbum()
            default:
                let alert = UIAlertController(title: "ERRO", message: "Você precisa autorizar acesso ao ábum para poder salvar sua foto", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func restart(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
}
























